#!/usr/bin/env python -tt
import sys
import numpy as np
import subprocess
import h5py

elements = ['', '']
lenels = len(elements)
dependence = 'deltag'
params = [0.5, 0.7, 1.0, 1.5, 2.0]
#params = [0.5, 0.7, 1.0, 1.25, 1.5, 1.75, 2.0, 2.1]
#params = [1.5, 2.0, 2.1]
#dependence = 'gammaR'
#params = [10, 12, 17, 20, 22, 25]
#dependence = 'theta'
#params = [1, 3, 5, 8, 10]

filename = dependence + '-dep_' + tail + '_' + sectail + '.dat'
f = open(filename, 'w')

globname = dependence + param + '_' + tail + '.glob_spec.h5'
g = h5py.File(globname, 'r')

for param in params:
    paramslist = []
    for element in elements:
        paramslist.append(g[element][0][0])
        #parlist.append(param)
    datastring = (lenels*'{:>17,.7E} ' + '{:>7}\n').format(*vellist)
    f.write(datastring),
    g.close()
    f.close()
# The output will be:
#
# gammaR   vR   vL   vRL   lumpeaksyn    lumpeakc
#
print 'DONE!'

def main():
    if len(sys.argv) != 3:
        sys.exit(' Usage: %s name-string tail\n' % (sys.argv[0]))
    glob_extrat(sys.argv[1],sys.argv[2],sys.argv[3])    
    
if __name__ == '__main__':
    main()
