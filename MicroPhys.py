#!/usr/bin/env python -tt

import sys
import subprocess as sp
import h5py

def DepFinder(dependence):
    if dependence == 'deltag':
        params = [0.5, 0.7, 1.0, 1.5, 2.0]
        #params = [0.5, 0.7, 1.0, 1.25, 1.5, 1.75, 2.0, 2.1]
        #params = [1.5, 2.0, 2.1]
    elif dependence == 'gammaR':
        params = [10, 12, 17, 20, 22, 25]
    elif dependence == 'theta':
        params = [1, 3, 5, 8, 10]
    else:
        print 'You did not enter a main parameter.',
        print "Try with 'deltag', 'gammaR' or 'theta' as it suits you...\n"
        exit(0)
    return params

params = DepFinder(sys.argv[1])
Bg_fname = sys.argv[1] + '-dep_' + sys.argv[2] + '.B-and-gamma.dat'
Bgf = open(Bg_fname, 'w')

i = 0
while i < len(params):
    bglist = []
    bglist.append(params[i])
    prename = sys.argv[1] + str(params[i]) + '_' + sys.argv[2] + '_pre.h5'
    prefile = h5py.File(prename, 'r')
    bglist.append(prefile['Bfs'][0][0])
    bglist.append(prefile['Brs'][0][0])
    bglist.append(prefile['g1f'][0][0])
    bglist.append(prefile['g1r'][0][0])
    bglist.append(prefile['g2f'][0][0])
    bglist.append(prefile['g2r'][0][0])
    datastring = '{:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>7}\n'.format(*bglist)
    Bgf.write(datastring),
    prefile.close()
    i += 1
Bgf.close()
# The output will be:
#
# Bfs  Bfr  g1f   g1r   g2f   g2r  param
#
print 'Bs data and gammas: FINISHED'
sp.call(["cp", Bg_fname, "/Volumes/scratch/git/PrmInt/Data/"])
sp.call(["ls", "-l", "/Volumes/scratch/git/PrmInt/Data/"])
