#!/usr/bin/env python -tt

import sys
import subprocess as sp
import h5py
import numpy as np

class Constants():
    """
    Constants Class:

    Physical constants are in CGS units
    """
    c_light = 2.99792458e+10
    m_e = 9.10938291e-28

print("""
IDEA!
  1. Shell script that gives me the paths
  2. Save paths in strings
  3. Acces paths from the script
  4. Pull and manipulate data without accessing to the directories
""")

#
# Reading the file with the working paths
#
def PathNames():
    path = []
    with open('workpaths.dat', r) as pwf:
        for line in iter(pwf.readline, ''):
            path.append(line)
    return path




# def PathFinder(dependence, magnet):
#     """
#     PathFinder(**args)

#     dependence:
#     magnet:
#     """
#     if magnet == 'S':
#         path
#     elif magnet == 'M':
#     elif magnet == 'W':
#     else:
#         exit("Wrong magnetization. Try with 'deltag', 'gammaR' or 'theta' as it suits you...\n")

#     if dependence == 'deltag':
#         params = [0.5, 0.7, 1.0, 1.5, 2.0]
#         #params = [0.5, 0.7, 1.0, 1.25, 1.5, 1.75, 2.0, 2.1]
#         #params = [1.5, 2.0, 2.1]
#         model = []
#         for i in params:
#             model.append(magnet+'-G10-D'+str(i)+'-T5')
#     elif dependence == 'gammaR':
#         params = [10, 12, 17, 20, 22, 25]
#         model = []
#         for i in params:
#             model.append(magnet+'-G'+str(i)+'-D1.0-T5')
#     elif dependence == 'theta':
#         params = [1, 3, 5, 8, 10]
#         model = []
#         for i in params:
#             model.append(magnet + '-G10-D1.0-T' + str(i))
#     else:
#         exit("Wrong name. Try with 'deltag', 'gammaR' or 'theta' as it suits you...\n")

#
# TODO: Do a loop through the three parameters insted one by one
#


#
# Dependency finder and labels
#
def DepFinder(dependence, magnet):
    """
    DepFinder(dependence, magnet)
    dependence:
    magnet:
    """
    if dependence == 'deltag':
        params = [0.5, 0.7, 1.0, 1.5, 2.0]
        #params = [0.5, 0.7, 1.0, 1.25, 1.5, 1.75, 2.0, 2.1]
        #params = [1.5, 2.0, 2.1]
        label = []
        for i in params:
            label.append(magnet+'-G10-D'+str(i)+'-T5')
    elif dependence == 'gammaR':
        params = [10, 12, 17, 20, 22, 25]
        label = []
        for i in params:
            label.append(magnet+'-G'+str(i)+'-D1.0-T5')
    elif dependence == 'theta':
        params = [1, 3, 5, 8, 10]
        label = []
        for i in params:
            label.append(magnet + '-G10-D1.0-T' + str(i))
    else:
        exit("Wrong name. Try with 'deltag', 'gammaR' or 'theta' as it suits you...\n")
    return params, label

def Fitting(filename, model, syn_pol_order, ic_pol_order):
    """
    Fitting()
    dependence:
    magnet:
    """
    #print '---> In Fitting'
    pf = h5py.File(filename + '.h5', 'r')
    nu_ds = pf['freqs']
    provnu = nu_ds[:]
    index_nuturn = np.nonzero(provnu == nuturn)[0][0] + 1
    gf.close()
    pf.close()
    
    specf = filename + '.tot_spec.dat'
    temp_nu, temp_SED = np.loadtxt(specf, usecols=(0, 2), unpack=True)
    nu = temp_nu[np.where(temp_nu < 10.**24)]
    nuF_nu = temp_SED[0:len(nu)]
    nusync = nu[:index_nuturn]
    nuic = nu[index_nuturn+1:]
    sed_sync = nuF_nu[:index_nuturn]
    sed_ic = nuF_nu[index_nuturn+1:]

    # >>>  Doing the polynomial fitting
    psyn, synres1, synres2, synres3, synres4 = np.polyfit(np.log10(nusync), np.log10(sed_sync), syn_pol_order, full=True)
    pic, icres1, icres2, icres3, icres4 = np.polyfit(np.log10(nuic), np.log10(sed_ic), ic_pol_order, full=True)

    FittingInGrace(nu, nusync, nuic, nuF_nu, psyn, pic, filename, model)
    
    return psyn, pic




    savef.write('{0} {1:14.8E} {2:14.8E} {3:14.8E} {4:14.8E} {5:14.8E} {6}\n'.format(*P_syn_output))


# ************************************************************
# ************************************************************
# ************************************************************

#
# Reading parameters
#

import pre_prams

PreParams(prefilename)

import pre_params

params = DepFinder(sys.argv[1])
while i < len(params):
    bglist.append(params[i])

gf = h5py.File(filename + '.glob_spec.h5', 'r')
nuturn_ds = gf['nuturn']
nuturn = nuturn_ds[0][0]

#
# Reading quantities
#
bglist = []
prename = sys.argv[1] + str(params[i]) + '_' + sys.argv[2] + '_pre.h5'
prefile = h5py.File(prename, 'r')
Bfs = prefile['Bfs'][0][0]
Brs = prefile['Brs'][0][0]

#
# Calculating quantities
#
sigmafs = Bfs/(4.*pi*rhofs*(Gfs*cLight)**2)
sigmars = Brs/(4.*pi*rhors*(Grs*cLight)**2)

csf_name = 'relevant_data.cfs'

#
# Writing quantities
#

with open(csf_name, 'w') as csfile:
    datastring = '{:>17,.7E}, {:>17,.7E}, {:>17,.7E}, {:>17,.7E}, {:>17,.7E}, {:>17,.7E}, {:>7}\n'.format(*bglist)
    csfile.write(datastring),
    prefile.close()
    i += 1

# ********************
# * Process complete *
# ********************
print 'CFS file: FINISHED'
sp.call(["cp", Bg_fname, "/Volumes/scratch/git/PrmInt/Data/"])
sp.call(["ls", "-l", "/Volumes/scratch/git/PrmInt/Data/"])

