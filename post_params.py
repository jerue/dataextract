#!/usr/bin/env python -tt

def PostParams(postfile):

    import os

    if (os.path.exists(postfile) != True):
        exit(postfile + ' does not exist in the current directory')

    import h5py

    elements = ['nuturn', 'nusyn', 'nuc', 'poly_nusyn', 'pol_ynuc']
    lenelements = len(elements)

    postlist = []
    postf = h5py.File(postfile, 'r')
    
    for element in elements:
        postlist.append(postf[element][0][0])
    postf.close()

    # post_params is the output of this function
            
    print 'Done with the extraction'

    return elements, post_params

    # datastring = (lenels*'{:>17,.7E} ' + '{:>7}\n').format(*paramslist)
    # f.write(datastring),
    # f.close()
