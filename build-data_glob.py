#!/usr/bin/env python -tt
#
# This code crates a data file with the next data:
#
#  >> Frequency ratio,
#  >> Flux ratio,
#  >> Synchrotron maximum frequency,
#  >> SSC maximum frequqncy,
#  >> Turning frequency,
#
# in the next order:
#
#    freqrat     fluxrat     nusyn     nuc     nuturn     deltag/Gamma_R/theta
#

import sys
import numpy as np
#import subprocess
import h5py

elements = ['freqrat', 'fluxrat', 'A_C', 'nusyn', 'nuc', 'nuturn']
extdat = '.dat'
exth5 = '.h5'

def DepFinder(dependence):
    if dependence == 'deltag':
        params = [0.5, 0.7, 1.0, 1.5, 2.0]
        #params = [0.5, 0.7, 1.0, 1.25, 1.5, 1.75, 2.0, 2.1]
        #params = [1.5, 2.0, 2.1]
    elif dependence == 'gammaR':
        params = [10, 12, 17, 20, 22, 25]
    elif dependence == 'theta':
        params = [1, 3, 5, 8, 10]
    else:
        print 'You did not enter a main parameter.',
        print "Try with 'deltag', 'gammaR' or 'theta' as it suits you...\n"
        exit(0)
    return params
    
def glob_file(dependence,tail):

    params = DepFinder(dependence)
    dataname = dependence + '-dep_' + tail + '_ratios' + extdat    
    coefsname = dependence + '-dep_' + tail + '_polycoefs' + extdat
    f = open(dataname, 'w')
    c = open(coefsname, 'w')
    headstring = '{:>17} {:>17} {:>17} {:>17} {:>17}\n'.format('# a0', 'a1', 'a2', 'a3', 'a4')
    c.write(headstring)

    i = 0
    while i < len(params):
        globlist = []
        globname = dependence + str(params[i]) + '_' + tail + '.glob_spec' + exth5
        globfile = h5py.File(globname, 'r')
        for el in elements:
            globel = globfile[el][0][0]
            globlist.append(globel)
        globlist.append(params[i])
        datastring = '{:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>7}\n'.format(*globlist)
        f.write(datastring),

        j = 0
        Syn_list = []
        IC_list = []
        while j <= 4:
            Syn_coefs = globfile['pol_syn'][j][0][0]
            IC_coefs = globfile['pol_IC'][j][0][0]
            Syn_list.insert(j, Syn_coefs)
            IC_list.insert(j, IC_coefs)
            j += 1
        Synstring = '{:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E}\n'.format(*Syn_list)
        ICstring = '{:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E}\n'.format(*IC_list)
        c.write('# '+dependence+' = '+str(params[i])+'\n')
        c.write(Synstring),
        c.write(ICstring),
        globfile.close()
        i += 1
    f.close()
    c.close()
    print 'DONE!'

def relative_velocity(dependence,tail):
    params = DepFinder(dependence)
    relavel_fname = dependence + '-dep_' + tail + '_relatvel' + extdat
    velf = open(relavel_fname, 'w')

    i = 0
    while i < len(params):
        vellist = []
        vellist.append(params[i])
        if dependence == 'gammaR':
            GammaR = float(params[i])
            Deltag = 1.
        elif dependence == 'deltag':
            GammaR = 10.
            Deltag = float(params[i])
        else:
            GammaR = 10.
            Deltag = 1.
        vR = np.sqrt(1. - 1./GammaR**2)
        vellist.append(vR)
        vL = np.sqrt(1. - 1./(GammaR**2*(1. + Deltag)))
        vellist.append(vL)
        vRL = (vL - vR)/(1. - vL*vR)
        vellist.append(vRL)
        velname = dependence + str(params[i]) + '_' + tail + '.glob_spec' + exth5
        velfile = h5py.File(velname, 'r')
        lumpeaksyn = velfile['lumpeaksyn'][0][0]
        vellist.append(lumpeaksyn)
        lumpeakc = velfile['lumpeakc'][0][0]
        vellist.append(lumpeakc)
        velfile.close()
        # gammaR   vR   vL   vRL   lumpeaksyn lumpeakc
        datastring = '{:>7} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E}\n'.format(*vellist)
        velf.write(datastring),
        i += 1
    velf.close()
    # The output will be:
    #
    # gammaR   vR   vL   vRL   lumpeaksyn    lumpeakc
    #
    print 'Relative velocity function: FINISHED'

def Bs_and_gammas(dependence, tail):
    params = DepFinder(dependence)
    Bg_fname = dependence + '-dep_' + tail + '_B-and-gamma' + extdat
    Bgf = open(Bg_fname, 'w')

    i = 0
    while i < len(params):
        bglist = []
        bglist.append(params[i])
        prename = dependence + str(params[i]) + '_' + tail + '_pre' + exth5
        prefile = h5py.File(prename, 'r')
        bglist.append(prefile['Bfs'][0][0])
        bglist.append(prefile['Brs'][0][0])
        bglist.append(prefile['g1f'][0][0])
        bglist.append(prefile['g1r'][0][0])
        bglist.append(prefile['g2f'][0][0])
        bglist.append(prefile['g2r'][0][0])
        # Bfs  Brs  g1f   g1r   g2f   g2r  param
        datastring = '{:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>17,.7E} {:>7}\n'.format(*bglist)
        Bgf.write(datastring),
        prefile.close()
        i += 1
    Bgf.close()
    # The output will be:
    #
    # param   Bfs  Bfr  g1f   g1r   g2f   g2r
    #
    print 'Bs data and gammas: FINISHED'

def main():
    if len(sys.argv) != 3:
        sys.exit(' Usage: %s name-string tail\n' % (sys.argv[0]))
    glob_file(sys.argv[1],sys.argv[2])
    relative_velocity(sys.argv[1],sys.argv[2])
    Bs_and_gammas(sys.argv[1], sys.argv[2])    
    
if __name__ == '__main__':
    main()
