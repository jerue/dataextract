#!/usr/bin/env python -tt
#import sys
#import numpy as np
#import subprocess

import h5py

elements = ['nfs', 'Q0fs', 'Bfs', 'nrs', 'Q0rs', 'Brs']
lenels = len(elements)
#dependence = 'deltag'
#params = [0.5, 0.7, 1.0, 1.5, 2.0]
#params = [0.5, 0.7, 1.0, 1.25, 1.5, 1.75, 2.0, 2.1]
#params = [1.5, 2.0, 2.1]
dependence = 'gammaR'
params = [10, 12, 17, 20, 22, 25]
#dependence = 'theta'
#params = [1, 3, 5, 8, 10]

tail = 'unmag'
sectail = 'nqb'

filename = dependence + '-dep_' + tail + '_' + sectail + '.dat'
f = open(filename, 'w')
datastring = ('{:<2}' + '{:>15} ' + (lenels-1)*'{:>17} ' + '{:>7}\n').format(*(['#'] + elements + [dependence]))
f.write(datastring),

for param in params:
    paramslist = []
    prename = dependence + str(param) + '_' + tail + '_pre.h5'
    p = h5py.File(prename, 'r')
    for element in elements:
        paramslist.append(p[element][0][0])
    p.close()
    paramslist.append(param)
    datastring = (lenels*'{:>17,.7E} ' + '{:>7}\n').format(*paramslist)
    f.write(datastring),
f.close()

print 'DONE!'

## def main():
##     if len(sys.argv) != 3:
##         sys.exit(' Usage: %s name-string tail\n' % (sys.argv[0]))
##     glob_extrat(sys.argv[1],sys.argv[2],sys.argv[3])    
    
## if __name__ == '__main__':
##     main()
